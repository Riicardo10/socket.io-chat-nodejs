var socket = io();

let params = new URLSearchParams( window.location.search );
if( !params.has( 'txt_nombre' ) || !params.has( 'txt_sala' ) ) {
    window.location = 'index.html';
    throw new Error( 'El nombre y la sala es necesari@ para entrar al chat' );
}
let usuario = {
    nombre: params.get( 'txt_nombre' ),
    sala: params.get( 'txt_sala' )
};

socket.on('connect', function() { 
    document.getElementById('txt_usuario_chat').innerHTML = 'Usuario: ' + usuario.nombre;
    
    document.getElementById('txt_sala_de_chat').innerHTML = usuario.sala;
    console.log('Conectado al servidor ' + usuario.nombre);   // browser
    socket.emit( 'entrarChat', usuario, function( resp ) {
        console.log( 'Usuarios conectados: ', resp );
        renderUsuarios( resp );
    } );
});

socket.on('disconnect', function() {
    console.log('Perdimos conexión con el servidor');
});

// socket.emit('enviarMensaje', {
//     nombre: 'Fernando',
//     mensaje: 'Hola Mundo'
// }, function(resp) {
//     console.log('respuesta server: ', resp);
// });

socket.on('crearMensaje', function( mensaje ) {
    //console.log('Servidor dice:', data);
    renderMensajes( mensaje, false );
});

// ESCUCHA -> LISTA DE PERSONAS CONECTADAS  CUANDO UN USUARIO SE CONECTA O SE DESCONECTA
socket.on('listaPersonas', function( usuarios ) {
    console.log( '=========================================' );
    console.log( 'USUARIOS ACTUALMENTE CONECTADOS:', usuarios );
    renderUsuarios( usuarios );
});

// ESCUCHA -> MENSAJES PRIVADOS
socket.on( 'mensajePrivado', function( mensaje ) {
    console.log( 'Mensaje privado: ' + mensaje.usuario );
    console.log( 'Mensaje privado: ' + mensaje.mensaje );
    console.log( 'Mensaje privado: ' + mensaje.fecha );
} );