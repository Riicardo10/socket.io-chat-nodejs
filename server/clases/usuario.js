class Usuario {
    constructor() {
        this.personas = [];
    }
    agregarPersona( id, nombre, sala ) {
        let persona = { id, nombre, sala };
        this.personas.push( persona );
        return this.personas;
    }
    getPersona( id ) {
        let persona = this.personas.filter( item => {
            return item.id === id
        } )[0];
        return persona;
    }
    getPersonas( ) {
        return this.personas;
    }
    getPersonasPorSala( sala ) {
        let personas_por_sala = this.personas.filter( item => {
            return item.sala === sala;
        } );
        return personas_por_sala;
    }
    eliminarPersona( id ) {
        let persona_eliminada = this.getPersona( id );
        this.personas = this.personas.filter( item => {
            return item.id != id
        } );
        return persona_eliminada;
    }
}

module.exports = {
    Usuario
};