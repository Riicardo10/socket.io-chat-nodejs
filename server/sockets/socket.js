const { io } = require('../server');
const { Usuario } = require( '../clases/usuario' );
const { crearMensaje } = require( '../utilidades/utilidades' );

const usuarios = new Usuario();

io.on( 'connection', (client) => {
    // CONEXION
    client.on( 'entrarChat', ( usuario, callback ) => {
        if( !usuario.nombre || !usuario.sala ) {
            return callback( {
                error: true,
                mensaje: 'Se esperaba nombre/sala de usuario.'
            } );
        }
        client.join( usuario.sala );
        usuarios.agregarPersona( client.id, usuario.nombre, usuario.sala );
        client.broadcast.to( usuario.sala ).emit( 'listaPersonas', usuarios.getPersonasPorSala( usuario.sala ) );
        client.broadcast.to( usuario.sala ).emit( 'crearMensaje', crearMensaje( 'Administrador2',  'El usuario ' + usuario.nombre + ' se conectó') );
        callback( usuarios.getPersonasPorSala( usuario.sala ) );
    } );
    // DESCONEXION
    client.on( 'disconnect', () => {
        let persona_eliminada = usuarios.eliminarPersona( client.id );
        client.broadcast.to( persona_eliminada.sala ).emit( 'crearMensaje', crearMensaje( 'Administrador',  'El usuario ' + persona_eliminada.nombre + ' se desconectó') );
        client.broadcast.to( persona_eliminada.sala ).emit( 'listaPersonas', usuarios.getPersonasPorSala( persona_eliminada.sala ) );
    } );
    // ESCUCHA DEL MENSAJE QUE MANDA EL USUARIO
    client.on( 'enviarMensaje', (data, callback) => {
        let persona = usuarios.getPersona( client.id );
        let mensaje = crearMensaje( persona.nombre, data.mensaje );
        client.broadcast.to( persona.sala ).emit( 'crearMensaje', mensaje );
        callback( mensaje );
    } );
    // MENSAJES PRIVADOS 
    client.on( 'mensajePrivado', (data) => {
        if(!data.mensaje){
            let persona = usuarios.getPersona( client.id );
            client.broadcast.to(data.para).emit( 'mensajePrivado', crearMensaje( persona.nombre, 'Err: Sin mensaje' ) );
        }
        else{
            let persona = usuarios.getPersona( client.id );
            client.broadcast.to(data.para).emit( 'mensajePrivado', crearMensaje( persona.nombre, data.mensaje ) );
        }
    } );
});
