let par = new URLSearchParams( window.location.search );
var usuario_chat = par.get( 'txt_nombre' );
var sala_chat = par.get( 'txt_sala' );

var contenedor_usuarios = $( '#divUsuarios' );
var contenedor_mensajes = $( '#divChatbox' );
var formulario_mensaje = $( '#formularioMensaje' );
var txt_mensaje = $( '#txt_mensaje' );

function renderUsuarios ( usuarios ) {    
    console.log( usuarios );
    var html = "";
    html += '<li>';
    html += '<a href="javascript:void(0)" class="active"> Chat de <span> ' + sala_chat + ' </span></a>';
    html += '</li>';

    for( var i=0; i<usuarios.length; i++ ) {
        html += '<li>';
        html += '<a data-id="' + usuarios[i].id +'" href="javascript:void(0)"><img src="assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>' + usuarios[i].nombre + '<small class="text-success">online</small></span></a>';
        html += '</li>';
    }

    contenedor_usuarios.html( html );
}

function renderMensajes ( mensaje, yo ) {
    var fecha = new Date( mensaje.fecha );
    var hora = fecha.getHours() + ':' + fecha.getMinutes();
    var html = "";
    var admin_class = 'info';
    if( mensaje.usuario === 'Administrador' ) {
        admin_class = 'danger'
    }
    else if( mensaje.usuario === 'Administrador2' ) {
        admin_class = 'success';
        mensaje.usuario = 'Administrador';
    }

    
    if( yo ) {
        html += '<li class="reverse">';
        html += '<div class="chat-content">';
        html += '<h5>' + mensaje.usuario + '</h5>';
        html += '<div class="box bg-light-inverse">' + mensaje.mensaje + '</div>';
        html += '</div>';
        html += '<div class="chat-img"><img src="assets/images/users/5.jpg" alt="user" /></div>';
        html += '<div class="chat-time">' + hora + '</div>';
        html += '</li>';
    }
    else {
        html += '<li class="animated fadeIn">';
        if( mensaje.usuario != 'Administrador' ){
            html += '<div class="chat-img"><img src="assets/images/users/1.jpg" alt="user" /></div>'
        }
        html += '<div class="chat-content">'
        html += '<h5>' + mensaje.usuario + '</h5>'
        html += '<div class="box bg-light-' + admin_class + '">' + mensaje.mensaje + '</div>'
        html += '</div>'
        html += '<div class="chat-time">' + hora + '</div>'
        html += '</li>';
    }
    contenedor_mensajes.append( html );
    scrollBottom();
}

function scrollBottom() {
    var newMessage = contenedor_mensajes.children('li:last-child');
    var clientHeight = contenedor_mensajes.prop('clientHeight');
    var scrollTop = contenedor_mensajes.prop('scrollTop');
    var scrollHeight = contenedor_mensajes.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight() || 0;
    if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
        contenedor_mensajes.scrollTop(scrollHeight);
    }
}

// =================================================================================
// LISTENERS
// =================================================================================

contenedor_usuarios.on( 'click', 'a', function() {
    var id = $( this ).data( 'id' );
    if( id ) {
        console.log( id );
    }
} );

formulario_mensaje.on( 'submit', function( event ) {
    event.preventDefault();             // no recarga la pagina
    if( txt_mensaje.val().trim().length != 0 ) {
        socket.emit('enviarMensaje', {
            nombre: usuario_chat,
            mensaje: txt_mensaje.val(),
            sala: sala_chat
        }, function( mensaje ) {
            renderMensajes( mensaje, true );
            txt_mensaje.val( '' ).focus();
        });
    }
} );